package eu.me73.luncheon.commons;

import ch.qos.logback.classic.Logger;
import java.io.IOException;
import java.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;


public class PricesUtilsTest {

    private static final Logger LOG = (Logger) LoggerFactory.getLogger(PricesUtilsTest.class);

    private static final double DEFAULT_EMPLOYEE = 1d;
    public static final double DEFAULT_PARTIAL = 2d;
    public static final double DEFAULT_VISITOR = 3d;
    private static final Prices DEFAULT_PRICES = new Prices(DEFAULT_EMPLOYEE, DEFAULT_PARTIAL, DEFAULT_VISITOR);
    private PricesUtils prices;

    @Before
    public void setup() throws IOException {
        prices = new PricesUtils(DEFAULT_PRICES);
        prices.parsePrices("../test-data/prices.ini");
    }

    @Test
    public void parsePrices() throws Exception {
        Assert.assertTrue(String.valueOf(prices.getPricesMap().size()),prices.getPricesMap().size() == 3);
    }

    @Test
    public void gainPricesDateBefore() throws Exception {
        Prices p = prices.gainPrices(LocalDate.MIN);
        Assert.assertNotNull(p);
        Assert.assertTrue(p.getEmployee() == DEFAULT_EMPLOYEE);
        Assert.assertTrue(p.getPartial() == DEFAULT_PARTIAL);
        Assert.assertTrue(p.getVisitor() == DEFAULT_VISITOR);
    }

    @Test
    public void gainPricesDateExact() throws Exception {
        Prices p = prices.gainPrices(LocalDate.of(2017, 3, 1));
        Assert.assertNotNull(p);
        Assert.assertTrue(p.getEmployee() == 1.50d);
        Assert.assertTrue(p.getPartial() == 1.90d);
        Assert.assertTrue(p.getVisitor() == 3.90d);
    }

    @Test
    public void gainPricesDateAfter() throws Exception {
        Prices p = prices.gainPrices(LocalDate.MAX);
        Assert.assertNotNull(p);
        Assert.assertTrue(p.getEmployee() == 1.99d);
        Assert.assertTrue(p.getPartial() == 2.99d);
        Assert.assertTrue(p.getVisitor() == 4.99d);
    }

    @Test
    public void gainPricesDateNotLoaded() throws Exception {
        LOG.debug("Unloading prices.");
        prices = null;
        prices = new PricesUtils(DEFAULT_PRICES);
        Prices p = prices.gainPrices(LocalDate.MIN);
        Assert.assertNotNull(p);
        Assert.assertTrue(p.getEmployee() == DEFAULT_EMPLOYEE);
        Assert.assertTrue(p.getPartial() == DEFAULT_PARTIAL);
        Assert.assertTrue(p.getVisitor() == DEFAULT_VISITOR);
    }
}