package eu.me73.luncheon.commons;

import ch.qos.logback.classic.Logger;
import java.io.IOException;
import java.time.LocalDate;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "luncheon")
public class LuncheonConfig {

    private static final Logger LOG = (Logger) LoggerFactory.getLogger(LuncheonConfig.class);

    private Integer ordering;
    private Integer year;
    private Double employee;
    private Double visitor;
    private Double partial;
    private String export;
    private String powerName;
    private String powerPassword;
    private String adminName;
    private String adminPassword;
    private Integer sameDayStartHour;
    private Integer sameDayStartMinutes;
    private Integer sameDayEndHour;
    private Integer sameDayEndMinutes;
    private String prices;
    private PricesUtils utils;
    private Prices defaultPrices;


    public LuncheonConfig() {
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getEmployee() {
        return employee;
    }

    public void setEmployee(Double employee) {
        this.employee = employee;
        this.defaultPrices.setEmployee(employee);
        this.utils.setDefaultPrices(defaultPrices);
    }

    public Double getVisitor() {
        return visitor;
    }

    public void setVisitor(Double visitor) {
        this.visitor = visitor;
        this.defaultPrices.setVisitor(visitor);
        this.utils.setDefaultPrices(defaultPrices);
    }

    public Double getPartial() {
        return partial;
    }

    public void setPartial(Double partial) {
        this.partial = partial;
        this.defaultPrices.setPartial(partial);
        this.utils.setDefaultPrices(defaultPrices);
    }

    public String getExport() {
        return export;
    }

    public void setExport(String exportName) {
        this.export = exportName;
    }

    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName;
    }

    public String getPowerPassword() {
        return powerPassword;
    }

    public void setPowerPassword(String powerPassword) {
        this.powerPassword = powerPassword;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public Integer getSameDayStartHour() {
        return sameDayStartHour;
    }

    public void setSameDayStartHour(Integer sameDayStartHour) {
        this.sameDayStartHour = sameDayStartHour;
    }

    public Integer getSameDayEndHour() {
        return sameDayEndHour;
    }

    public void setSameDayEndHour(Integer sameDayEndHour) {
        this.sameDayEndHour = sameDayEndHour;
    }

    public Integer getSameDayStartMinutes() {
        return sameDayStartMinutes;
    }

    public void setSameDayStartMinutes(Integer sameDayStartMinutes) {
        this.sameDayStartMinutes = sameDayStartMinutes;
    }

    public Integer getSameDayEndMinutes() {
        return sameDayEndMinutes;
    }

    public void setSameDayEndMinutes(Integer sameDayEndMinutes) {
        this.sameDayEndMinutes = sameDayEndMinutes;
    }

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.defaultPrices = new Prices(getEmployee(), getPartial(), getVisitor());
        this.utils = new PricesUtils(defaultPrices);
        this.prices = prices;
        try {
            this.utils.parsePrices(prices);
        } catch (IOException e) {
            LOG.warn("Prices from file: {} cannot be read.", prices);
        }
    }

    public Prices gainPrices(LocalDate date) {
        return this.utils.gainPrices(date);
    }
}
