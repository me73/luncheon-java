package eu.me73.luncheon.commons;

import static eu.me73.luncheon.commons.DummyConfig.createBufferedReaderFromFileName;

import ch.qos.logback.classic.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import org.slf4j.LoggerFactory;

public class PricesUtils {

    private static final Logger LOG = (Logger) LoggerFactory.getLogger(PricesUtils.class);
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH);
    private Map<LocalDate, Prices> pricesMap = new HashMap<>();
    private Prices defaultPrices;

    public PricesUtils(final Prices defaultPrices) {
        this.defaultPrices = defaultPrices;
    }

    private PricesUtils() {
    }

    public void parsePrices(final String prices) throws IOException {
        pricesMap.clear();
        BufferedReader pricesFile;
        pricesFile = createBufferedReaderFromFileName(prices);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Importing prices from file {} ", prices);
        }
        String line;
        while (Objects.nonNull(line = pricesFile.readLine())) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Read line {}", line);
            }
            final String[] s = line.split(";");
            if (s.length >= 4) {
                LocalDate date = LocalDate.parse(s[0], FORMATTER);
                String ss[];
                double e = 0;
                double p = 0;
                double v = 0;
                for (int i=1;i<4;i++) {
                    ss = s[i].split("=");
                    if (ss.length > 1) {
                        if (ss[0].toUpperCase().equals("EMPLOYEE")) {
                            e = Double.parseDouble(ss[1]);
                        } else {
                            if (ss[0].toUpperCase().equals("PARTIAL")) {
                                p = Double.parseDouble(ss[1]);
                            } else {
                                v = Double.parseDouble(ss[1]);
                            }
                        }
                    }
                }
                pricesMap.put(date,new Prices(e,p,v));
            }
        }
    }

    public Prices gainPrices(final LocalDate date) {
        if (pricesMap.isEmpty()) {
            LOG.warn("Prices are not loaded.");
            return defaultPrices;
        } else {
            if (pricesMap.containsKey(date)) {
                return pricesMap.get(date);
            } else {
                Prices tmpPrices = new Prices(defaultPrices);
                for (Map.Entry<LocalDate, Prices> entry : pricesMap.entrySet()) {
                    if (entry.getKey().isBefore(date)) {
                        tmpPrices = entry.getValue();
                    }
                }
                return tmpPrices;
            }
        }
    }

    public Map<LocalDate, Prices> getPricesMap() {
        return pricesMap;
    }

    public void setDefaultPrices(final Prices defaultPrices) {
        this.defaultPrices = defaultPrices;
    }
}
