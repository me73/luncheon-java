package eu.me73.luncheon.commons;

public class LuncheonConstants {

    public static String USER_ORDERS_EMPTY = "User orders collection is empty nothing to store";
    public static String USER_ORDERS_WRONG_USER = "Cannot store this order you are not power user";
    public static String USER_ORDERS_NOTHING_TO_STORE = "Nothing to store";

}
