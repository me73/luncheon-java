package eu.me73.luncheon.commons;

public class Prices {

    private Double employee;
    private Double visitor;
    private Double partial;

    public Prices(Double employee, Double partial, Double visitor) {
        this.employee = employee;
        this.visitor = visitor;
        this.partial = partial;
    }

    public Prices(Prices prices) {
        this.employee = prices.getEmployee();
        this.partial = prices.getPartial();
        this.visitor = prices.getVisitor();
    }

    public Double getEmployee() {
        return employee;
    }

    public Double getVisitor() {
        return visitor;
    }

    public Double getPartial() {
        return partial;
    }

    public void setEmployee(Double employee) {
        this.employee = employee;
    }

    public void setVisitor(Double visitor) {
        this.visitor = visitor;
    }

    public void setPartial(Double partial) {
        this.partial = partial;
    }
}
